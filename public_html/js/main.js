;
"use strict";

function nl2br( str ) {	// Inserts HTML line breaks before all newlines in a string
    //
    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)

    return str.replace(/([^>])\n/g, '$1<br/>');
}

(function ($) {

    function initDeleteFileClick(ev) {
        ev.preventDefault();
        var $li = $(ev.target).parents('li').first();
        var url = $li.attr('data-delete');

        initDeleteEvent(ev, url, $li);
    }

    /** Delete Event function **/
    function initDeleteEvent(ev, url, $li) {
        ev.preventDefault();
        $li.addClass('hoverto_remove');
        var it = ev.target;

        //display confirm message
        swal({
            title: "Do you whant to delete this file?",
            text: "You can not revoke this action",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, Delete it',
            cancelButtonText: 'No',
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(
            function () {
                //run deleting process
                $('<div class="loading alert alert-danger">Deleting...</div>').show().prependTo($li);
                var dataArray = {};
                dataArray._method = 'DELETE';

                // call script to delete file
                $.post(url, dataArray, function (result) {
                    // if result is   success
                    if ( result.code == 1) {

                        var $uploader = $li.parents('.fileuploader').data('FileUploader');
                        $li.hide('slow', function () {
                            $li.remove();
                            setTimeout(function () {
                                // var res = $uploader.displayControls();
                                $uploader.checkControlOnVisible();
                            }, 500);
                        });

                    } else {
                        // if has fail result
                        $li.find('loading').remove();
                        var msg = 'Connection error';
                        if (result.msg) {
                            msg = result.msg;
                        }
                        var $errorConnection = $('<ul class="alert alert-danger"><li>' + msg + '</li></ul>');
                        $errorConnection.hide().prependTo($li);
                        $errorConnection.show('slow');
                        $li.removeClass('hoverto_remove');
                        setTimeout(function () {
                            $errorConnection.hide('slow', function () {
                                $errorConnection.remove();
                            })
                        }, 5000);
                    }
                }, 'json')
                    .fail(function () {
                        $('.loading', $li).remove();
                        var $errorConnection = $('<ul class="alert alert-danger"><li>Connection Error</li></ul>');
                        $errorConnection.hide().prependTo($li);
                        $errorConnection.show('slow');
                        $li.removeClass('hoverto_remove');
                        setTimeout(function () {
                            $errorConnection.hide('slow', function () {
                                $errorConnection.remove();
                            })
                        }, 5000);
                    });

            },
            // if user make dismiss
            function (dismiss) {
                // dismiss can be 'cancel', 'overlay',
                if (dismiss === 'cancel') {

                }

                $li.removeClass('hoverto_remove');
                return false;
            });
    }

    function initFileUploader() {


        var $uploadFile = $('#upload_file'),
            urlUploadFile = $('#upload_file').data('url2upload');

        if ($uploadFile.length==0) return;

        var fileUploaderSettings = {
            url: urlUploadFile,
            ajaxFormParams: {
                'rec_id': $('#rec_id').length ? $('#rec_id').val() : ($uploadFile.attr('data-rec_id').length ? $uploadFile.attr('data-rec_id') : ''),
                'model': $('#model').length ? $('#model').val() : ($uploadFile.data('model').length ? $uploadFile.data('model') : ''),
            },
            fileElementSelector: '.uploadfile', // input[type=file]
            listAddedFilesSelector: '.addfileslist', // list of files for uploading
            listExistsFiles: '.existsfiles', //list of files already uploaded
            btnAddFilesSelector: '.fileinput-button', // BTN select file
            dropzoneSelector: '.dropzone', // dropzone area
            imagePreview: true,
            imagesOnly: true,
            countMax: 1,
            // callback function after init Uploader
            callbackInit: function () {

            },
            rowTpl4Files: '',
            rowTpl4Images: '<li>' +
                '<div class="imgcolumn"></div><span class="file_size"></span><span class="file_name"></span>' +
                '<button type="button" title="__Delete__MSG__" class="btn btn-danger btn-xs btn-delete">X</button>' +
                '<div class="progress">' +
                '<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0; min-width: 2em;">0%</div>' +
                '</div>' +
                '<div class="error"></div>' +
                '<button  type="button" class="btn btn-default btn-xs btn-upload-one" title="__Upload__MSG__" ><span class="glyphicon glyphicon-upload"></span><span class="btn-title">__Upload__MSG__</span></button>' +
                '</li>',
            // callback function on Handle Files
            callbackBeforeHandleFiles: function (files) {

            },
            callbackAfterHandleFiles: function (files) {
            },
            // callback function on Add each file
            callbackAddedFile: function (file) {

            },
            callbackUploadSuccess: function (response, $row, $uploader, uid, fileName, fileSize, fileExt) {
                if (response.code != 1) {

                } else {
                    // save file id to hidden field
                    var $result_el = $uploader.$el.find('.add_file_id');
                    if ($result_el.length) {
                        var content = $result_el.val();
                        if (content.length == 0) {
                            $result_el.val('' + response.file.id + "/" + response.file.uid);
                        } else {
                            $result_el.val(content + ',' + response.file.id + "/" + response.file.uid);
                        }
                    }
                    var $deleteBtn = $('<button class="btn btn-danger btn-sm btn-delete-file">X</button>').click(initDeleteFileClick);
                    // display result
                    var $newLi = $('<li data-id="' + response.file.id + '" data-uid="' + response.file.uid + '" data-delete="' + response.file.url_delete + '"></li>');
                    if (response.file.type == 'image') {
                        $newLi.append('<a href="' + response.file.url + '" target="_blank"><img src="' + response.file.url + '?type=sm" class=""></a>');
                    } else {
                        $newLi.append('<div class="file_ext">' + response.file.ext + '</div><a href="' + response.file.url + '" target="_blank">' + response.file.basename + '</a>');
                    }
                    $newLi.append($deleteBtn);
                    $uploader.$el.find($uploader.options.listExistsFiles).append($newLi);
                    $row.remove();

                }
            },
            msg: {
                too_big_file: ("File size to big for uploading (Max size 2Mb)"),
                Upload: ("Upload"),
                Delete: ("Delete")
            },
        };
        $('.btn-delete-file').click(initDeleteFileClick);
        $('#upload_file').FileUploader(fileUploaderSettings);
    }


    function initPreviewBtn() {
        $('#btnPreview').click(function(ev) {
            ev.preventDefault();
            var $img = $('.existsfiles img').first();
            $('#preview_name').html( $('#name').val() );
            $('#preview_status').html( $('#is_done').val()=='1'?'Done':'-' );
            $('#preview_email').html( $('#email').val() );
            $('#preview_text').html( nl2br($('#text').val()) );
            $('#preview_img').html( $img.clone() );


            $('.preview').show('slow');
            $('#taskForm').hide('show');
        });
        $('#btnPreviewClose').click(function (ev){
            ev.preventDefault();
            $('.preview').hide('slow');
            $('#taskForm').show('show');
        })
    }
    $(document).ready(function ($) {

        initFileUploader();
        initPreviewBtn();
        // auto hide animation for messages
        setTimeout(function () {
            $('.autohide').hide(1000);
        }, 3000)

        // admin approve btn
        $('.btn-approve').click(function (ev) {
            var $lnk = $(this);
            ev.preventDefault();

            $.post(this.href,
                {is_done: (!parseInt($(this).attr('data-is_done'))) ? 1 : 0},
                function (response) {

                    if (response.code == 1) {
                        if (response.is_done == 1) {
                            $lnk.html('Done');
                        } else {
                            $lnk.html('-');
                        }
                        $lnk.attr('data-is_done', response.is_done);
                    } else {
                        alert(response.alert);
                    }
                }, 'json').fail(function () {
                alert('Error connection to server');
            });

        });

    });
})(jQuery);
