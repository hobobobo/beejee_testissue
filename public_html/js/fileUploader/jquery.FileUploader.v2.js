;
"use strict";

(function ($) {

    $(document).ready(function ($) {

    });

    $.fn.FileUploader = function (options, arg1) {
        if (typeof options == "string") {
            if (options == "getInstance") {
                return $(this).data('FileUploader');
            }
            if (options == "getOptions") {
                return $(this).data('FileUploader').options;
            }
            if (options == "addFile") {
                return $(this).data('FileUploader').addFile(arg1);
            }

            return;
        }
        return this.each(function () {
            (new $.FileUploader(this, options));
        });
    };
    $.FileUploaderDefaultSetting = {};

    $.FileUploader = function (el, opts) {
        var self = this;
        self.el = el;
        self.$el = $(el);

        self.options = {
            url: 'upload.php',
            ajaxFormParams: {},
            fileElementSelector: '.uploadfile',
            dropzoneSelector: '.dropzone',
            listAddedFilesSelector: '.addfileslist',
            listExistsFiles: '.existsfiles',
            progressBarValueSelector: '.progress-bar',
            progressBarSelector: '.progress',
            btnDeleteSelector: '.btn-delete',
            btnUploadOneSelector: '.btn-upload-one',
            btnUploadAllSelector: '.btn-upload-all',
            rowErrorSelector: '.error',
            cssMainClass4Uploader: 'fileuploader',
            cssDropZoneOnHover: 'dropzone-active',
            cssFileExt: 'file_ext',
            cssFileSize: 'file_size',
            cssFileName: 'file_name',
            cssBtnUploadOne_title: 'btn-title',
            imagesOnly: false,
            accept: 'image.*',
            imagePreview: true,
            // autoUpload: false,
            countMax: -1,
            maxFileSizeInBytes: 2000000,
            msg: {
                too_big_file: "File size to big for uploading (Max size 2Mb)",
                Upload: "Upload",
                Delete: "Delete"
            },
            rowTpl4Images: '<li>' +
            '                   <div class="imgcolumn"></div>(<span class="file_size"></span>)<span class="file_name"></span>' +
            '                   <button type="button" title="__Delete__MSG__" class="btn btn-danger btn-xs btn-delete"><span class="glyphicon glyphicon-remove"></span></button>' +
            '                   <br>' +
            '                   <div class="progress">' +
            '                       <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0; min-width: 2em;">0%</div>' +
            '                   </div>' +
            '                   <span class="error"></span>' +
            '                   <button  type="button" class="btn btn-default btn-xs btn-upload-one" title="__Upload__MSG__" ><span class="glyphicon glyphicon-upload"></span><span class="btn-title">__Upload__MSG__</span></button>' +
            '               </li>',
            rowTpl4Files: '<li>' +
            '                   <div class="file_ext"></div>(<span class="file_size"></span>)<span class="file_name"></span>' +
            '                   <button type="button" title="__Delete__MSG__" class="btn btn-danger btn-xs btn-delete"><span class="glyphicon glyphicon-remove"></span></button>' +
            '                   <br>' +
            '                   <div class="progress">' +
            '                       <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0; min-width: 2em;">0%</div>' +
            '                   </div>' +
            '                   <span class="error"></span>' +
            '                   <button  type="button" class="btn btn-default btn-xs btn-upload-one" title="__Upload__MSG__" ><span class="glyphicon glyphicon-upload"></span><span class="btn-title">__Upload__MSG__</span></button>' +
            '               </li>',

            // callback function after init Uploader
            callbackInit: function () {

            },
            // callback function on Handle Files
            callbackBeforeHandleFiles: function (files) {

            },
            // callback function on Handle Files
            callbackAfterHandleFiles: function (files) {
            },

            // callback function on Add each file
            callbackAddedFile: function () {

            },
            callbackDeletedRow: function () {

            },
            callbackUploadSuccess: function () {

            },
            callbackDrawRow: function (file, uploader, fileExtension, humanFileSize, isOkFileSize) {
                var $row,
                    uid = file.uid,
                    $domPart,
                    readyToUpload  = true;

                var isImage = (fileExtension == 'jpg' || fileExtension == 'epg' || fileExtension == 'png' || fileExtension == 'gif'  );
                if (file.type.match(self.options.accept)) {
                    isImage = true;
                }

                if (uploader.options.imagesOnly || (isImage && uploader.options.imagePreview )) {
                    $row = $(uploader.options.rowTpl4Images);

                }else {
                    $row = $(uploader.options.rowTpl4Files);
                }

                $row.addClass('file_' + uid).attr('data-file_index', uid);
                if (uploader.options.cssFileExt) {
                    $domPart = $row.find('.' + uploader.options.cssFileExt);
                    if ($domPart.length) {
                        $domPart.html(fileExtension);
                    }
                }
                if (uploader.options.cssFileSize) {
                    $domPart = $row.find('.' + uploader.options.cssFileSize);
                    if ($domPart.length) {
                        $domPart.html(humanFileSize);
                    }
                }
                if (uploader.options.cssFileName) {
                    $domPart = $row.find('.' + uploader.options.cssFileName);
                    if ($domPart.length) {
                        $domPart.html(encodeURI(file.name));
                    }
                }

                if (uploader.options.btnDeleteSelector) {
                    $domPart = $row.find(uploader.options.btnDeleteSelector);
                    if ($domPart.length) {
                        $domPart.click(
                            (function (uid) {
                                return function (ev) {
                                    ev.preventDefault();
                                    ev.stopPropagation();
                                    self.deleteRow(uid);
                                }
                            })(uid)
                        ).attr('title', uploader.options.msg.Delete);
                    }
                }

                if (uploader.options.btnUploadOneSelector) {
                    $domPart = $row.find( uploader.options.btnUploadOneSelector );
                    $domPart.click(
                        (function (uid) {
                            return function (ev) {
                                ev.preventDefault();
                                ev.stopPropagation();
                                self.uploadFile(uid);

                            }
                        })(uid)
                    ).attr('title', uploader.options.msg.Upload);
                }
                if (uploader.options.cssBtnUploadOne_title) {
                    $domPart = $row.find('.' + uploader.options.cssBtnUploadOne_title);
                    if ($domPart.length) {
                        $domPart.html(uploader.options.msg.Upload);
                    }
                }

                if (uploader.options.maxFileSizeInBytes && file.size > uploader.options.maxFileSizeInBytes) {
                    $row.find( uploader.options.rowErrorSelector ).addClass('alert', 'alert-danger').html( uploader.options.msg.too_big_file ).show();
                    $row.find( uploader.options.btnUploadOneSelector ).remove();
                    readyToUpload = false;
                }

                var $listAddedFiles = uploader.$el.find(uploader.options.listAddedFilesSelector);
                $listAddedFiles.append($row);

                // show Upload All button
                if (readyToUpload) {
                    $(self.options.btnUploadAllSelector, self.$el).show();
                }


                var isImage = (fileExtension == 'jpg' || fileExtension == 'epg' || fileExtension == 'png' || fileExtension == 'gif'  );
                if (uploader.options.imagesOnly || (isImage && uploader.options.imagePreview )) {
                    var reader = new FileReader();
                    reader.onload = (function (theFile, uploader, $row) {
                        return function (e) {
                            var img = '<img src="' + e.target.result + '" class="thumb">';
                            $row.find('.imgcolumn').html(img);
                            // uploader.$el.find(uploader.options.listAddedFilesSelector).append($row);
                        }
                    })(file, uploader, $listAddedFiles.find(' .file_' + uid));
                    // Read in the image file as a data URL.
                    reader.readAsDataURL(file);
                }else {

                }

            },
            callbackCheckCanUploadNewFile: function ($el, options) {
                return true;
            },
            callbackGetCountOfExistsFiles: function ($el, options, uploader) {
                return $el.find( options.listExistsFiles ).children().length
            }
        };
        self.options = $.extend({}, self.options, $.FileUploaderDefaultSetting, opts);

        self.rowFileListTag = '';
        // storage for files
        self.filesInList = {};
        // for unique index
        self.fileIndex = -1;

        // Add a reverse reference to the DOM object
        if (typeof self.$el.data("FileUploader") == "undefined") {
            self.$el.data("FileUploader", self);
        }

        self.init = function () {
            self.$el.addClass(self.options.cssMainClass4Uploader);
            // init + Browse Files btn
            if (self.options.fileElementSelector != "") {
                self.$el.find(self.options.fileElementSelector).change(
                    function () {
                        self.handleFiles(this.files)
                    }
                );
            }

            // init Drop Zone
            self.initDropZone();
            // init UploadAll Btn
            self.initBtnUploadAll();

            // run callback event
            if (typeof self.options.callbackInit == "function") {
                self.options.callbackInit();
            }
        };

        self.initBtnUploadAll = function (){
            // btn Upload All
            if (self.options.btnUploadAllSelector != "") {
                $(self.options.btnUploadAllSelector, self.$el).click(function () {
                    self.$el.find(self.options.listAddedFilesSelector + ' li').not('[data-finished="true"]').each(function () {
                        self.uploadFile($(this).data('file_index'));
                    });
                    $(self.options.btnUploadAllSelector, self.$el).hide('slow');
                })
            }
            if (self.options.countMax > 1) {
                $(self.$el.find(self.options.fileElementSelector)).prop('multiple', 'multiple');
            } else {
                self.$el.find(self.options.fileElementSelector).removeAttr('multiple');
            }

            self.checkBtnUploadAllOnVisible();
        };
        self.initDropZone = function () {
            // init Drop Zone
            if (self.options.dropzoneSelector != "") {
                var $dropzone = $(self.options.dropzoneSelector);

                $dropzone.bind( // #drop-block блок куда мы будем перетаскивать наши файлы
                    'dragenter',
                    function (e) {
                        // Действия при входе курсора с файлами  в блок.
                        $dropzone.addClass(self.options.cssDropZoneOnHover);
                        e.preventDefault();
                        if (e.stopPropagation) {
                            e.stopPropagation();
                        }

                        return false;
                    }).bind(
                    'dragover',
                    function (e) {
                        // Действия при перемещении курсора с файлами над блоком.
                        e.preventDefault();
                        if (e.stopPropagation) {
                            e.stopPropagation();
                        }
                        $dropzone.addClass(self.options.cssDropZoneOnHover);
                        return false;
                    }).bind(
                    'dragleave',
                    function (e) {
                        // Действия при перемещении курсора с файлами над блоком.
                        e.preventDefault();
                        if (e.stopPropagation) {
                            e.stopPropagation();
                        }
                        $dropzone.removeClass(self.options.cssDropZoneOnHover);
                        return false;
                    }).bind(
                    'drop',
                    function (e) { // Действия при «вбросе» файлов в блок.
                        $dropzone.removeClass(self.options.cssDropZoneOnHover);
                        e.preventDefault();
                        if (e.stopPropagation) {
                            e.stopPropagation();
                        }
                        //e.originalEvent.dataTransfer.getData('text/html');
                        self.handleFiles(e.originalEvent.dataTransfer.files)
                    });
            }
        };
        self.handleFiles = function (fileList) {
            // callback event
            if (typeof self.options.callbackBeforeHandleFiles== "function") {
                self.options.callbackBeforeHandleFiles(fileList);
            }

            for (var i = 0, f; f = fileList[i]; i++) {
                if (self.canAddNewFileForUpload()) {
                    self.addFile(f);
                }
            }

            if (typeof self.options.callbackAfterHandleFiles== "function") {
                self.options.callbackAfterHandleFiles(fileList);
            }
        };

        self.addFile = function (fileObj) {

            // Only process image files.
            if (self.options.imagesOnly) {
                if (!fileObj.type.match(self.options.accept)) {
                    alert(fileObj.name + ' is not image');
                    return false;
                }
            }

            // create unique index for each received files
            self.fileIndex++;
            fileObj.uid = self.fileIndex;
            self.filesInList['file_' + self.fileIndex] = fileObj;
            // var extension = self.getFileExtension3(fileObj.name);

            if (typeof self.options.callbackAddedFile == "function") {
                self.options.callbackAddedFile(fileObj, self);
            }
            if (typeof self.options.callbackDrawRow == "function") {
               self.options.callbackDrawRow(fileObj, self, self.getFileExtension3(fileObj.name), self.humanFileSize(fileObj.size, true), self.checkOnMaxFileSize(fileObj));
            }

            self.checkControlOnVisible();
        };

        self.deleteRow = function (uid) {
            delete  self.filesInList['file_' + uid];
            self.$el.find(self.options.listAddedFilesSelector + ' .file_' + uid).hide('slow', function () {
                $(this).remove();
                self.checkControlOnVisible();
                self.checkBtnUploadAllOnVisible();
            });

            if( typeof self.options.callbackDeletedRow == "function") {
                self.options.callbackDeletedRow();
            }
        };

        self.checkBtnUploadAllOnVisible = function () {
            var rowTagName = $(self.options.rowTpl4Files).prop('tagName');

            if (self.$el.find(self.options.listAddedFilesSelector + ' ' + rowTagName ).not('[data-finished="true"]').length <= 1) {
                $(self.options.btnUploadAllSelector, self.$el).hide('slow');
            }
            rowTagName = $(self.options.rowTpl4Images).prop('tagName');
            if (self.$el.find(self.options.listAddedFilesSelector + ' ' + rowTagName ).not('[data-finished="true"]').length <= 1) {
                $(self.options.btnUploadAllSelector, self.$el).hide('slow');
            }
        };

        self.getCountOfExistsFiles = function (){
            if (self.options.callbackGetCountOfExistsFiles) {
                return self.options.callbackGetCountOfExistsFiles(self.$el, self.options, self);
            }
            return 0;
        };
        self.canAddNewFileForUpload = function (){
            if (self.options.countMax <=0) {
               return true;
            }
            if ( self.options.countMax <=Object.keys(self.filesInList).length + self.getCountOfExistsFiles() ) {
                return false;
            }
            return true;
        };
        self.checkControlOnVisible = function () {

            if (self.options.countMax >0 && self.options.countMax <= Object.keys(self.filesInList).length) {
                self.$el.find(self.options.btnAddFilesSelector).hide();
                if (self.options.countMax == 1) {
                    $(self.options.btnUploadAllSelector).hide();
                }
            } else {
                self.$el.find(self.options.btnAddFilesSelector).show();
                if (self.options.countMax == 1) {
                    $(self.options.btnUploadAllSelector).show();
                }
            }
        };
        self.checkOnMaxFileSize = function (fileObj) {
            return (self.options.maxFileSizeInBytes && fileObj.size > self.options.maxFileSizeInBytes)
        };

        self.getFileExtension3 = function (filename) {
            return filename.slice((filename.lastIndexOf(".") - 1 >>> 0) + 2).toLowerCase();
        };
        self.humanFileSize = function (bytes, si) {
            if (typeof si == "undefined") si = true;
            var thresh = si ? 1000 : 1024;
            if (bytes < thresh) return bytes + ' B';
            var units = si ? ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'] : ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];
            var u = -1;
            do {
                bytes /= thresh;
                ++u;
            } while (bytes >= thresh);
            return bytes.toFixed(1) + ' ' + units[u];
        };

        self.uploadFile = function (uid) {
            var $row = self.$el.find(self.options.listAddedFilesSelector + ' .file_' + uid);

            // not standard situation
            if ($row.data('finished') == 'true') {
//                alert('This file already uploaded');
                $row.find(self.options.rowErrorSelector).html('This file already uploaded').show();
                return;
            }

            // not standard situation
            if (typeof self.filesInList['file_' + uid] == "undefined") {
//                alert('Please delete this item and try again');
                $row.find(self.options.rowErrorSelector).html('Please delete this item and try again').show();
                return;
            }

            var updateProgress = (function (uid, $row/*, uploader*/) {
                return function (percentage) {
                    $row.find(self.options.progressBarValueSelector).attr('aria-valuenow', percentage).width(percentage + '%').html(percentage + '%');
                }
            })(uid, $row/*, self*/);

            updateProgress(0);
            $row.find(self.options.progressBarSelector).show();
            $row.find(self.options.btnDeleteSelector).hide();
            $row.find(self.options.btnUploadOneSelector).hide();
            $row.find(self.options.rowErrorSelector).html("").hide();

            self.checkBtnUploadAllOnVisible();

            // create form with file
            var formData = new FormData();
            for (var key in self.options.ajaxFormParams) {
                formData.append(key, self.options.ajaxFormParams[key]);
            }

            formData.append('file', self.filesInList['file_' + uid]);


            $.ajax({
                url: self.options.url, // point to server-side PHP script
                dataType: 'json',  // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: formData,
                type: 'post',
                enctype: 'multipart/form-data',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                xhr: function () {
                    var myXhr = $.ajaxSettings.xhr();
                    if (myXhr.upload) {
                        myXhr.upload.addEventListener('progress', function (e) {
                            if (e.lengthComputable) {
                                var max = e.total;
                                var current = e.loaded;
                                var Percentage = (current * 100) / max;
                                var width = Math.ceil(e.loaded / e.total * 100);// + '%'

                                if (Percentage >= 100) {
                                    // process completed
                                    $row.attr('data-finished', 'true');
                                }
                                updateProgress(width);
                            }
                        }, false);
                    }
                    return myXhr;
                },
                success: function (response) {
                    if (response.code == 1) {
                        // display response from the PHP script, if any
                        $row.attr('data-finished', 'true');
                        $row.attr('data-response_id', response.file.id);
                        $row.attr('data-response', response.file);
                        var fileName = self.filesInList['file_' + uid].name;
                        var fileSize = self.humanFileSize(self.filesInList['file_' + uid].size);
                        var fileExt = self.getFileExtension3(self.filesInList['file_' + uid].name);
                        delete self.filesInList['file_' + uid];

                        if( typeof self.options.callbackUploadSuccess == "function") {
                            self.options.callbackUploadSuccess(response, $row, self, uid, fileName, fileSize, fileExt);
                        }

                        self.showOrHideUploader();

                    } else {
                        $row.find(self.options.btnDeleteSelector).show();
                        $row.find(self.options.btnUploadOneSelector).show();
                        $row.find(self.options.progressBarSelector).hide();
                        updateProgress(0);
                        console.log('fail uid: ' + uid);
                        $row.find(self.options.rowErrorSelector).html( 'Error uploading file: ' + data.status + " " + data.statusText ).show();
                        $(self.options.btnUploadAllSelector, self.$el).show('slow');
                    }

                },
                error: function (data) {
                    $row.find(self.options.btnDeleteSelector).show();
                    $row.find(self.options.btnUploadOneSelector).show();
                    $row.find(self.options.progressBarSelector).hide();
                    updateProgress(0);
                    console.log('fail uid: ' + uid);
                    $row.find(self.options.rowErrorSelector).html( 'Error uploading file: ' + data.status + " " + data.statusText ).show();
                    $(self.options.btnUploadAllSelector, self.$el).show('slow');
                }
            });
        };


        self.showOrHideUploader = function () {
            if ( !self.options.callbackCheckCanUploadNewFile(self.$el, self.options)) {
                self.$el.find(self.options.dropzoneSelector).hide();
                // check on max count
                self.checkControlOnVisible();
                return false;
            } else {
                self.$el.find(self.options.dropzoneSelector).show();
                // check on max count
                self.checkControlOnVisible();
                return true;
            }
        };

        self.init();
    };

})(jQuery);
