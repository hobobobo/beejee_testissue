<?php

@session_start();
error_reporting(E_ALL);
ini_set('display_errors', 1);
@header("Content-type: text/html; charset=UTF-8");

include_once '../beejee/libs/vardump.inc.php';
include_once '../beejee/Autoloader.php';
include_once '../beejee/App/Helpers/core.php';
include_once '../beejee/Core/Db.php';

$dbConfig = include_once '../beejee/App/config_db.inc.php';

\Core\Db::getInstance($dbConfig);


$app = new \Core\App();
$app->run();
