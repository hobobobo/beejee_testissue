<?php

namespace Core;

/**
 * Class Auth
 * This is wrapper class for authentication process.
 * We can save here user information in session or in db or something else
 * We can make authorization in stack for CRM systems
 * For different entry points can be self authorization ($currentEntry)
 *
 * @package Core
 */
class Auth
{

    /**
     * @var self
     */
    static $instance;
    // entry point for multi authorization
    private $currentEntry = 'frontend';
    // array of multy authorized users
    // so user can be authorized like seller and buyer in same time
    private $authUserAccountsArray = array();

    // auth session key
    private $keySession;

    function __construct()
    {
        $this->keySession = md5(__FILE__);

        if (!$this->authUserAccountsArray) {
            // load data about authorization
            $this->load();
        }
    }

    // save authorization data
    // if needs storage session data in Database we can do it here
    public function save()
    {
        // serialize data in session
        \Core\Session::set($this->keySession, serialize($this->authUserAccountsArray));
    }

    /**
     * Restore authentificate information
     */
    private function load()
    {
        // unserialize data from session
        $tmp = \Core\Session::get($this->keySession);
        if ($tmp) {
            $this->authUserAccountsArray = unserialize($tmp);
        }
    }

    public function isLogined()
    {
        $user = $this->getUser();
        return isset($user['login']) && $user['login'];
    }

    /**
     * Auth user by login name
     * @param string $login
     * @param array $userData (permissions, specific user data, etc)
     */
    public function login($login, $userData = array())
    {
        if (!$login) return;

        // here user can be specific model
        $currentUser = array();
        $currentUser['login'] = $login;
        $currentUser['userData'] = $userData;
        $this->authUserAccountsArray[$this->currentEntry] = $currentUser;
        $this->save();
    }


    /**
     * Get current User Info forom Auth Storage
     * @return array
     */
    public function getUser()
    {
        if (isset($this->authUserAccountsArray[$this->currentEntry])) {
            return $this->authUserAccountsArray[$this->currentEntry];
        }
        return array();
    }

    /**
     * Get user information from Auth Storage
     * @return bool|mixed
     */
    public function getUserData()
    {
        $user = $this->getUser();
        if (!$user) return false;

        return $user['userData'];
    }

    /**
     * @return Auth
     */

    static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * destroy all user data for specific entry point in current session
     */
    public function logout()
    {
        if (isset($this->authUserAccountsArray[$this->currentEntry])) {
            unset($this->authUserAccountsArray[$this->currentEntry]);
            $this->save();
        }
    }

}