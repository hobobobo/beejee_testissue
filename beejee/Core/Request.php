<?php

namespace Core;

/**
 * Class Request
 * @package Core
 */
class Request
{
    // auto trim all incoming data
    private $trim = true;

    /**
     * Get param from $_GET
     * @param $name
     * @return bool|string
     */
    public function __get($name)
    {
        $var = self::post($name, $this->trim);
        return $var ? $var : self::get($name, $this->trim);
    }

    /**
     * get param from $_POST
     * @param string $name
     * @param bool $trim
     * @return bool|string
     */
    static public function post($name, $trim = true)
    {
        if (isset($_POST[$name])) return $trim ? trim($_POST[$name]) : $_POST[$name];
        return false;
    }

    static public function get($name, $trim = true)
    {
        if (isset($_GET[$name])) return $trim ? trim($_GET[$name]) : $_GET[$name];
        return false;
    }

    /**
     * get parem from $_POST or $_GET
     * @param $name
     * @param bool $trim
     * @return bool|string
     */
    static function param($name, $trim = true)
    {
        $var = self::post($name, $trim);
        return $var ? $var : self::get($name, $trim);
    }

    /**
     * Check is AJAX Request!
     * @return bool
     */
    static function isAjaxRequest()
    {
        $headers = array_change_key_case(self::getAllHeaders(), CASE_LOWER);
        return ((isset($headers['x-requested-with']) && $headers['x-requested-with'] == 'XMLHttpRequest') || (isset($headers['x-request']) && $headers['x-request'] == 'JSON') || (isset($_GET['ajax']) && $_GET['ajax']) || (isset($_POST['ajax']) && $_POST['ajax'])) ? true : false;
    }


    /**
     * Get All Headers
     * @return array
     */
    static function getAllHeaders()
    {
        if (function_exists('getallheaders')) return getallheaders();
        if (function_exists('apache_request_headers')) return apache_request_headers();
        $headers = array();
        foreach ($_SERVER as $name => $value) {
            if (substr($name, 0, 5) == 'HTTP_') $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
        }
        return $headers;
    }

}