<?php

namespace Core;

use \PDO;

/**
 * Class Db
 * @package Core
 */
class Db extends PDO
{
    /**
     * @var Db
     */
    static private $instance;


    public function __construct($dbConfig)
    {
        // connect to DB
        $opt = array(
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
        );
        $db_connection = $dbConfig['DB_CONNECTION'];
        if (!isset($dbConfig[$db_connection])) {
            $db_connection = 'mysql';
        }
        $db_params = $dbConfig[$db_connection];
        switch ($db_connection) {
            case 'sqlsrv':
                $dsn = $db_connection . ':Server=' . $db_params['host'] . ';Database=' . $db_params['database'];
                $otp[PDO::SQLSRV_ATTR_ENCODING] = PDO::SQLSRV_ENCODING_UTF8;
                break;
            case 'mysql':
            default:
                $dsn = $db_connection . ':host=' . $db_params['host'] . ';dbname=' . $db_params['database'] . ';charset=' . $db_params['charset'];
        }


        parent::__construct($dsn, $db_params['username'], $db_params['password'], $opt);
        switch ($db_connection) {
            case 'sqlsrv':

                break;
            case 'mysql':
            default:
                parent::exec("set names utf8");
        }

        // its look strange but there no other solution for version of php on my VDS server =)
        self::$instance = $this;
    }

    /**
     * singletron
     * @return PDO
     */
    public static function getInstance($dbConfig = array())
    {
        if (!self::$instance) {
            return new self($dbConfig);
        }
        return self::$instance;
    }


}