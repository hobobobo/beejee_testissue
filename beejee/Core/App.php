<?php

namespace Core;


class App
{
    public function __construct()
    {

    }

    /**
     * Main function to process request
     *
     * @param $request
     * @return mixed
     */
    public function dispatcher($request)
    {
        list($controller, $action) = explode('/', $request);

        $controller = ucfirst($controller);
        $controller = '\\App\\Controllers\\' . ucfirst($controller) . 'Controller';
        $action = $action . 'Action';
        $obj = new  $controller();
        if (method_exists($obj, $action)) {
            $result = $obj->$action();
        } else {

            // TODO: in future make good solution for 404 page
            $this->error404('method ' . $controller . '::' . $action . ' not found');
        }
        return $result;
    }


    /**
     * Main function
     */
    public function run()
    {
        // start output buffering
        ob_start();

        $router = \Core\Router::getInstance();

        // detect rule in router
        $request = $router->searchRuleInRouter();

        // get data from buffer (it is should be empty but we will display it for developer,
        // may be there debug messages | errors)

        // catch trash|debug messages remove trash from AJAX responces
        $trashErrorsDebugsData = ob_get_contents();
        ob_clean();
        $result = $this->dispatcher($request);

        // catch trash|debug messages remove trash from AJAX responces
        $trashErrorsDebugsData .= ob_get_contents();
        ob_clean();

        // check is Ajax
        if (Request::isAjaxRequest()) {
            // output ajax response
            echo $result;
            die();
        } else {

            // add debug messages to result
            if (isset($_SESSION['onlyForMe']) && $trashErrorsDebugsData) {
                $result .= $trashErrorsDebugsData;
            }
            // output layout
            $res = View::render('layout', array('mainContent' => $result));
            echo $res;
        }
        die(); // done
    }

    static public function error404($message)
    {
        header("HTTP/1.0 404 Not Found");
        die('404 Page not found <br>' . $message);
    }
}