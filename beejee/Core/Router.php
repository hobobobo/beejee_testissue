<?php

namespace Core;


/**
 * Class Router
 *
 * @package Core
 */
class Router
{
    /**
     * @var Router
     */
    static $instance;

    /**
     * Current SiteURL
     * @var string
     */
    static $siteUrl;

    /**
     * current request to site
     * @var string
     */
    static $requestURI;


    /**
     *  Rules Lists
     * @var array
     */
    private $rulesArray = array();


    private $defaultRule = 'home/index'; // for root "/"

    private function __construct()
    {
        // load rules
        $this->rulesArray = include \Core\Autoloader::config('routers');

        // make default rule if rules not Exists
        if (!$this->rulesArray) {
            $this->addRoute('/', $this->defaultRule);
        }
    }

    /**
     * Singelton pattern
     * @return Router
     */
    static function getInstance()
    {
        if (self::$instance) {
            return self::$instance;
        }
        return new self();
    }

    /**
     * Add new Rule to router
     * @param $routeKey
     * @param $routerRule
     * @return void
     */
    public function addRoute($routeKey, $routerRule)
    {
        $this->rulesArray[$routeKey] = $routerRule;
    }

    /**
     * Search rule in router list for requestURI
     * if requestURI is null will be userd URI from URL
     *
     * @param null | string $requestURI
     * @return mixed
     */
    public function searchRuleInRouter($requestURI = NULL)
    {
        if (!$requestURI) {
            // detect RequestURI from URL
            $requestURI = $this->getRequest4Router();
        }

        foreach ($this->rulesArray as $routerRule) {
            $key = key($routerRule);
            if ($key == $requestURI || '/' . $key == $requestURI) {
                return reset($routerRule);
            }
        }

        $this->generate404Error($requestURI);
        die();
    }

    /**
     *  Get Request URI without subfolders if site placed into it
     *  and if its multilanguage site without languages in url
     * @return string
     */
    public function getRequestFromUri()
    {
        if (self::$requestURI) return self::$requestURI;
        // detect correct uri for site when it placed in subfolder
        // so we removed subfolder from URI request
        $tmp = $_SERVER['DOCUMENT_ROOT'] . $_SERVER['REQUEST_URI'];
        self::$requestURI = substr($tmp, strlen(dirname($_SERVER['SCRIPT_FILENAME'])));

        // Here we can detect language from URL  for multilanguage application
        return self::$requestURI;
    }


    /**
     * get Site Url
     * @return string
     */
    static public function getSiteUrl()
    {
        if (self::$siteUrl) return self::$siteUrl;

        $script_path = $_SERVER['SCRIPT_NAME'];
        $script_path = dirname($script_path);
        $script_path = str_replace(DIRECTORY_SEPARATOR, '/', $script_path);
        if (isset($_SERVER['HTTP_HOST'])) {
            $siteUrl = '//' . $_SERVER['HTTP_HOST'] . '/' . ltrim($script_path, '/') . '/';
        } else {
            $siteUrl = '/' . ltrim($script_path, '/') . '/';
        }
        self::$siteUrl = rtrim($siteUrl, '/') . '/';

        return self::$siteUrl;
    }


    /**
     * get Request without params
     * @return mixed
     */
    public function getRequest4Router()
    {
        $result = explode('?', $this->getRequestFromUri());
        return $result[0];
    }


    /**
     * Search rules by name
     * @param $name
     * @return bool|mixed
     */
    public function getRuleByName($name)
    {
        return isset($this->rulesArray[$name]) ? $this->rulesArray[$name] : false;
    }

    /**
     * Generate 404 Error
     * Templorary solution.
     * TODO: make goood sulution
     * @param $requestURI
     */
    public function generate404Error($requestURI)
    {
        @header('HTTP/1.1 404 Not Found');
        die('404 Page Not Found (' . $requestURI . ')');
    }
}