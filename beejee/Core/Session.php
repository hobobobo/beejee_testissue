<?php

namespace Core;

/**
 * Class Session
 * @desc Its wrapper for session. Its allow setup several instance of application on one domain.
 * Each application will have self storage for session data
 * @package Core
 */
class Session
{
    /**
     * Save variable in session
     * @param string $varName
     * @param mixed $value
     */
    public function __set($varName, $value)
    {
        self::set($varName, $value);
    }

    /**
     * Get variable from session
     * @param $varName
     * @return bool
     */
    public function __get($varName)
    {
        return self::get($varName);
    }

    /**
     * Save variable in session
     * @param string $varName
     * @param mixed $value
     */
    static public function set($varName, $value)
    {
        $_SESSION[md5(__FILE__)][$varName] = $value;
    }

    /**
     * Get variable from session
     * @param $varName
     * @return bool
     */
    static public function get($varName)
    {
        if (isset($_SESSION[md5(__FILE__)][$varName]))
            return $_SESSION[md5(__FILE__)][$varName];
        return false;
    }
}