<?php

namespace Core;


class View
{
    /**
     * Render tpl
     * We can  change template engine here
     *
     * @param string $tplName
     * @param array $vars
     * @return string
     */
    static public function render($tplName, $vars)
    {
        extract($vars);
        $path2Tpl = \Core\Autoloader::view($tplName);
        $res = '';
        if ($path2Tpl) {
            // native php tpl =)
            include_once $path2Tpl;
            $res = ob_get_contents();
            ob_clean();
        }
        return $res;
    }
}