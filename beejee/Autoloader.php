<?php

namespace Core;

class Autoloader
{

    static $MAIN_DIR;

    public static function register()
    {
        if (!Autoloader::$MAIN_DIR) {
            Autoloader::$MAIN_DIR = __DIR__;
        }

        spl_autoload_register(function ($class) {
            // search in main dir first
            $file = str_replace('\\', DIRECTORY_SEPARATOR, $class) . '.php';
            if (file_exists(Autoloader::$MAIN_DIR . DIRECTORY_SEPARATOR . $file)) {
                require Autoloader::$MAIN_DIR . DIRECTORY_SEPARATOR . $file;
                return true;
            }
            return false;
        });
    }

    /**
     * get full path to View file
     * @param $tplName
     * @return string|bool
     */
    public static function view($tplName)
    {
        $tplPath = self::$MAIN_DIR . '/App/Views/' . $tplName . '.tpl.php';
        if (file_exists($tplPath)) {
            return $tplPath;
        }
        return false;
    }

    /**
     * get full path to config file
     * @param $configName
     * @return string
     */
    public static function config($configName)
    {
        $tplPath = self::$MAIN_DIR . '/App/' . $configName . '.config.php';
        if (file_exists($tplPath)) {
            return $tplPath;
        }
        return false;
    }
}

\Core\Autoloader::register();