<?php


/**
 * Helper function to build link in view
 * @param $routeName
 * @return string
 */
function route($routeName)
{
    $res = \Core\Router::getInstance()->getRuleByName($routeName);
    return $res ? \Core\Router::getSiteUrl() . key($res) : '#' . $routeName;
}

/**
 * Helper function to make correct url to assets
 * @param $link
 * @return string
 */
function asset($link)
{
    return \Core\Router::getSiteUrl() . $link;
}

/**
 * Redirect function
 * @param $url
 * @param bool $bUseJS
 */
function Redirect($url, $bUseJS = false)
{

    if (headers_sent() || $bUseJS)
        print '<script type="text/javascript" lang="JavaScript">location="' . $url . '";</script><noscript>Please folow this link <a href="' . $url . '">' . $url . '</a> <p>We also recommend to turn on JavaScript in your browser for comfortable work.</a></noscript>';
    else
        header('location: ' . $url);
    exit();
}

/**
 * Helper function to build html of errors block
 * @param $errors
 * @return string
 */
function errorAlertsBlock($errors)
{
    if (!$errors) return '';

    return '<div class="alert alert-danger" role="alert">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                <span class="sr-only">Error:</span>' . implode('<br>', $errors) . '
            </div>';
}