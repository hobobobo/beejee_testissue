<?php

namespace App\Models;

use Core\DB;
use PDO;

class TasksModel
{
    static $tableName = 'tasks';

    /**
     *  Add new record
     * @param $name
     * @param $email
     * @param $text
     * @return bool|string
     */
    public static function add($name, $email, $text)
    {
        $db = Db::getInstance();

        $paramsInQuery = array();
        $paramsInQuery['name'] = $name;
        $paramsInQuery['email'] = $email;
        $paramsInQuery['text'] = $text;

        $paramsInQuery['created_at'] = date('Y-m-d H:i:s');  // NOW();

        $sql = 'insert into ' . self::$tableName . ' (`name`, `email`, `text`,   `created_at`) 
                                        values (:name, :email, :text,  :created_at)';
        $stm = $db->prepare($sql);
        $res = $stm->execute($paramsInQuery);
        if ($res) {
            return $db->lastInsertId();
        }
        return $res;
    }

    /**
     * update exists record
     * @param $id
     * @param $dataArray
     * @return bool
     */
    static public function update($id, $dataArray)
    {
        $id = (int)$id;
        $db = DB::getInstance();

        // allowed fields to change data
        $fieldsArray = array(
            'name',
            'email',
            'text',
            'img',
            'is_done',
        );

        $paramsInQuery = array();
        $str = '';

        // bind only allowed fields
        foreach ($fieldsArray as $element) {
            if (!isset($dataArray[$element])) continue;
            $str .= ' `' . $element . '` =:' . $element . ', ';
            $paramsInQuery[$element] = $dataArray[$element];
        }
        $str = rtrim($str, ', ');
        $sql = 'update ' . self::$tableName . ' set ' . $str . ' where id= :id';

        $stm = $db->prepare($sql);
        $paramsInQuery['id'] = $id;
        return $stm->execute($paramsInQuery);
    }


    /**
     * Get one record
     * @param $id
     * @return mixed
     */
    static public function get($id)
    {
        $id = (int)$id;
        $db = DB::getInstance();
        $sql = 'select * from `' . self::$tableName . '` where id= :id';
        $stm = $db->prepare($sql);
        $paramsInQuery = array();
        $paramsInQuery['id'] = $id;
        $stm->execute($paramsInQuery);
        return $stm->fetch();
    }

    /**
     * Get Total rows
     * @return mixed
     */
    static public function getCountRows()
    {
        $sql = 'SELECT count(*) as cnt from `' . self::$tableName . '` ';
        return DB::getInstance()->query($sql)->fetchColumn();
    }

    /**
     * Get found rows for SQL_CALC_FOUND_ROWS
     * @return mixed
     */
    static public function getCalcFoundRows()
    {
        $sql = 'SELECT FOUND_ROWS() as found_rows';
        return DB::getInstance()->query($sql)->fetch();
    }


    /**
     * get List of Records
     * @param string $order
     * @param bool $orderIsAsc
     * @param int $from
     * @param int $recOnPage
     * @return array
     */
    static public function getList($order = 'name', $orderIsAsc = true, $from = 0, $recOnPage = 3)
    {
        // check order on allowed values
        $allowedOrderField = array('name', 'email', 'is_done');
        if (!in_array($order, $allowedOrderField)) {
            $order = 'name';
        }

        $orderDirection = $orderIsAsc ? 'asc' : 'desc';

        $from = (int)$from;
        $recOnPage = (int)$recOnPage;


        $paramsInQuery = array();

        $db = DB::getInstance();
        $sql = 'select * from `' . self::$tableName . '` order by ' . $order . ' ' . $orderDirection . ' limit ' . $from . ', ' . $recOnPage;

        $stm = $db->prepare($sql);
        $stm->execute($paramsInQuery);
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }


    /**
     * Attach file
     * @param $id
     * @param $filename
     * @return bool
     */
    static function attachFile($id, $filename)
    {
        $db = DB::getInstance();
        $sql = 'update ' . self::$tableName . ' set img = :img where id= :id';
        $stm = $db->prepare($sql);
        $paramsInQuery = array();
        $paramsInQuery['id'] = $id;
        $paramsInQuery['img'] = $filename;
        return $stm->execute($paramsInQuery);
    }
}