<?php

namespace App\Controllers;

use Core\Autoloader;
use Core\Request;

/**
 * Class FileController
 * @decr  I try to make all operation with file in FileController but have not time to finish main idea
 *        so some code not elegant
 *
 * @package App\Controllers
 */
class FileController extends Controller
{
    private $uploadDir = 'Uploads';

    public function __construct()
    {
        parent::__construct();

        // its bad solution but I have not time to make it beautiful
        $this->uploadDir = \Core\Autoloader::$MAIN_DIR . '/' . $this->uploadDir;
    }

    /**
     * Action to delete file
     * @return string json
     */
    public function deleteAction()
    {
        $file = Request::param('id');
        $uploads_dir = $this->uploadDir;
        list($file) = explode('?', $file);
        // delete file on disk
        if (file_exists($uploads_dir . '/' . $file)) {
            unlink($uploads_dir . '/' . $file);
        }

        // its bad solution too but I have not time to make it beautiful :)
        $id = Request::param('rec_id');
        $owner = Request::param('model');
        if ($owner && $id) {
            switch ($owner) {
                case 'task':
                    // delete file from task
                    \App\Models\TasksModel::attachFile($id, '');
                    break;
            }
        }

        $result = array();
        $result['code'] = 1; // successfull
        echo json_encode($result);
        die();
    }

    /**
     * Read user files
     * All files stored upper then public_html so Its gateway to work with file
     */
    public function readAction()
    {
        $file = Request::param('id');
        $uploads_dir = $this->uploadDir;
        list($file) = explode('?', $file);
        if (file_exists($uploads_dir . '/' . $file)) {

            // clear output buffer
            if (ob_get_level()) {
                ob_end_clean();
            }


            header('Content-Type: ' . mime_content_type($uploads_dir . '/' . $file));
            header('Content-Disposition: inline; filename="' . ($file) . '"');
            header('Content-Length: ' . filesize($uploads_dir . '/' . $file));
            readfile($uploads_dir . '/' . $file);
            die();
        } else {
            $this->show404('File ' . $file . ' not found');
            die();
        }
    }

    /**
     * Upload file
     *
     * @throws \Gumlet\ImageResizeException
     */
    public function uploadAction()
    {
        $id = Request::post('rec_id');
        $owner = Request::post('model');

        $uploads_dir = $this->uploadDir;

        if (isset($_FILES["file"]) && $_FILES["file"]["error"] == UPLOAD_ERR_OK) {

            $tmp_name = $_FILES["file"]["tmp_name"];

            // basename() to safe from attack;
            $originalFileName = basename($_FILES["file"]["name"]);

            // create unique name
            $name = '' . uniqid(date("YmdHis_")) . '_' . $originalFileName;


            $destination = $uploads_dir . '/' . $name;
            move_uploaded_file($tmp_name, $destination);

            // auto resize
            include_once Autoloader::$MAIN_DIR . '/libs/imageResize/ImageResize.php';
            $image = new \Gumlet\ImageResize($destination);
            $image->resizeToBestFit(320, 240);
            $image->save($destination);

            $mime = mime_content_type($destination);
            $type = substr($mime, 0, strpos($mime, '/'));

            // its bad solution too but I have not time to make it beautiful :)
            // Its used for ajax uploading in edit form
            if ($owner && $id) {
                switch ($owner) {
                    case 'task':
                        \App\Models\TasksModel::attachFile($id, $name);
                        break;
                }
            }

            $result = array();
            $result['code'] = 1;
            $result['file'] = array('id' => $name,
                'uid' => md5($name . 'hash'),
                'basename' => $originalFileName,
                'filename' => $originalFileName,
                'ext' => pathinfo($name, PATHINFO_EXTENSION),
                'type' => $type,
                'mime' => $mime,
                'url' => route('file.read') . '?id=' . $name,
                'url_delete' => route('file.delete') . '?id=' . $name
            );

            echo json_encode($result);
            die();
        }

        $result = array();
        $result['code'] = 0;
        echo json_encode($result);
    }
}