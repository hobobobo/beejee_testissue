<?php

/***
 *  It should be Rest API controller, but time is limited so I make it more simple
 *
 */

namespace App\Controllers;

use Core\Request;
use Core\View;
use Core\Auth;

class UserController extends Controller
{

    /**
     * display login form
     * @return string
     */
    public function loginAction()
    {

        $errorMessages = array();

        $login = '';

        // save data if its POST
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $login = Request::post('login');
            $password = Request::post('password');

            if (!$login) {
                $errorMessages[] = 'Login field is required';
            }

            if (!$password) {
                $errorMessages[] = 'Password field is required';
            }

            $configAuth = include \Core\Autoloader::config('auth');
            if (!$errorMessages && ($configAuth['login'] != $login || $configAuth['password'] != $password)) {
                $errorMessages[] = 'User not found';
            }
            if (!$errorMessages) {
                // make authorization
                Auth::getInstance()->login($login);
                Redirect(route('root') . '?success=auth');
            }


        }

        // display create form
        $res = View::render('login', array(
            'errorMessages' => $errorMessages,
            'login' => $login,
        ));
        return $res;
    }

    /**
     * logout Action
     */
    public function logoutAction()
    {
        Auth::getInstance()->logout();
        Redirect(route('login') . '');

    }


}