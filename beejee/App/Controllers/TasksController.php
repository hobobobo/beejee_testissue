<?php

/***
 *  It should be Rest API controller, but time is limited so I make it more simple
 *
 */
//


namespace App\Controllers;

use Core\Request;
use Core\View;
use Core\Auth;

use App\Models\TasksModel;

class TasksController extends Controller
{

    /**
     * return list of tasts
     */
    public function indexAction()
    {
        $order = Request::get('order');
        $from = Request::get('from');
        $orderIsAsc = Request::get('order_direction');

        // set default $orderIsAsc
        if ($orderIsAsc === false) {
            $orderIsAsc = 1;
        }

        // check order variable on posible values
        $allowedOrderField = array('name', 'email', 'is_done');
        if (!in_array($order, $allowedOrderField)) {
            $order = 'name';
        }

        $recOnPage = 3;
        $tasks = TasksModel::getList($order, $orderIsAsc, $from * $recOnPage, $recOnPage);
        $totalRows = TasksModel::getCountRows();


        return View::render('task.list',
            array(
                'tasks' => $tasks,
                'totalPages' => ceil($totalRows / $recOnPage),
                'from' => $from,
                'order' => $order,
                'order_direction' => $orderIsAsc,
            ));
    }

    /**
     * display create task form
     * @return string
     */
    public function createAction()
    {
        // array for errors collection
        $errorMessages = array();

        $name = '';
        $email = '';
        $text = '';

        // save data if  POST
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $name = Request::post('name');
            $email = Request::post('email');
            $text = Request::post('text');
            $addedFiles = Request::post('added_file');

            // validation
            if (!$name) {
                $errorMessages[] = 'Name field is required';
            }

            if (!$text) {
                $errorMessages[] = 'Text field is required';
            }
            if (!$email) {
                $errorMessages[] = 'Email field is required';
            } elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $errorMessages[] = 'Email is not correct';
            }

            if (!$errorMessages) {
                // add new record
                $id = TasksModel::add($name, $email, $text);

                // save attached files
                if ($id && $addedFiles) {

                    // we got ids of uploaded files separated by coma
                    $addedFiles = explode(',', $addedFiles);
                    foreach ($addedFiles as $file) {
                        if (!$file) continue;

                        // each ID goes along with the hash id/hash
                        // so we can check it here
                        $tmp = explode('/', $file);
                        $file_id = $tmp[0];

                        TasksModel::attachFile($id, $file_id);
                    }
                }

                if ($id) {
                    Redirect(route('root') . '?success=added');
                } else {
                    Redirect(route('root') . '?fail=added');
                }
            }
        }

        // display create form
        $res = View::render('task.create', array(
            'foo' => 'bar',
            'errorMessages' => $errorMessages,
            'name' => $name,
            'email' => $email,
            'text' => $text,
        ));
        return $res;
    }

    /**
     * edit task form
     * allowed only for Admin
     * Authorization should checked before the dispatcher's work begins
     */
    public function editAction()
    {
        // check authorization
        if (!Auth::getInstance()->isLogined()) {
            Redirect(route('login'));
        }

        $id = (int)Request::param('id');
        if (!$id) {
            $this->show404('Task not found');
        }

        $errorMessages = array();
        if ($_SERVER['REQUEST_METHOD'] == 'POST') { //update data in database

            $name = Request::post('name');
            $email = Request::post('email');
            $text = Request::post('text');

            // validation
            if (!$name) {
                $errorMessages[] = 'Name field is required';
            }

            if (!$text) {
                $errorMessages[] = 'Text field is required';
            }
            if (!$email) {
                $errorMessages[] = 'Email field is required';
            } elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $errorMessages[] = 'Email is not correct';
            }
            $dataArray = array();
            $dataArray['name'] = $name;
            $dataArray['email'] = $email;
            $dataArray['text'] = $text;


            if (!$errorMessages) {
                // update record into DB
                TasksModel::update($id, $dataArray);
                Redirect(route('root') . '?success=added');
            }
            $task = $dataArray;
        } else {
            // get task info
            $task = TasksModel::get($id);
            if (!$task) {
                $this->show404('Task not found');
            }
        }

        // display edit form
        $res = View::render('task.edit', array(
            'errorMessages' => $errorMessages,
            'dataArray' => $task,
            'id' => $id,
        ));
        return $res;
    }


    /**
     * Change Status
     * allowed only for Admin
     * Authorization should checked before the dispatcher's work begins
     * @return string json
     */
    public function statusToggleAction()
    {
        $id = (int)Request::param('id');
        $isDone = (int)Request::param('is_done');

        if (!$id) {
            $response['code'] = 0;
            $response['alert'] = 'Unknow record. Please refrash the page';
            return json_encode($response);
        }

        $response = array();

        // Authorization should checked before the dispatcher's work begins
        if (Auth::getInstance()->isLogined()) {
            TasksModel::update($id, array('is_done' => $isDone));
            $response['code'] = 1;
            $response['is_done'] = $isDone;

        } else {
            $response['code'] = 0;
            $response['alert'] = 'Authorization is required';
            $response['redirect'] = route('login');
            $response['action'] = 'redirect';
        }
        return json_encode($response);
    }
}