<?php
/**
 * Created by PhpStorm.
 * User: Zanoza
 * Date: 30.05.2018
 * Time: 15:42
 */

namespace App\Controllers;


class Controller
{
    /**
     * @var \Core\Request
     */
    protected $request;

    public function __construct()
    {
        $this->request = new \Core\Request();
    }

    // TODO: make good solution in future
    public function showAccessDenied()
    {
        header("HTTP/1.1 403 Forbidden");
        die('Access denied');
    }

    // TODO: make good solution
    public function show404($message = '')
    {
        header("HTTP/1.0 404 Not Found");
        die('404 Page not found<br>' . $message);
    }

}