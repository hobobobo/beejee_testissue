<?php

return array(
    // name => array( uri => controller/action )
    'root' => array( ''=>'tasks/index' ) ,
    'login'=>array('login'=>'user/login'),
    'logout'=>array('logout'=>'user/logout'),
    'task' => array( 'task'=>'tasks/create'),
    'file.upload'=>array('file/upload'=>'file/upload'),
    'file.read'=>array('file/read'=>'file/read'),
    'file.delete'=>array('file/delete'=>'file/delete'),
    'task.approve'=> array('task/approve'=>'tasks/statusToggle'),
    'task.edit'=> array('task/edit'=>'tasks/edit'),

);