<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Task list</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?= asset( 'css/bootstrap.min.css') ?>"
          integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="<?= asset( 'js/swal/sweetalert2.min.css') ?>" rel="stylesheet">
    <link href="<?= asset( 'js/fileUploader/fileUploader.css') ?>" rel="stylesheet">
    <link href="<?= asset( 'css/theme.css') ?>" rel="stylesheet">
</head>
<body>
<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top mainnav" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="<?= route('root'); ?>">Task list</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="<?= route('task') ?>">Add Task</a>
                </li>
                <?php

                    if(\Core\Auth::getInstance()->isLogined()) :
                ?>
                        <li class="nav-item">
                            <a class="nav-link js-scroll-trigger" href="<?= route('logout') ?>">Logout</a>
                        </li>
                <?php
                    else :
                ?>
                        <li class="nav-item">
                           <a class="nav-link js-scroll-trigger" href="<?= route('login') ?>">Login</a>
                        </li>
                <?php
                    endif;
                ?>
            </ul>
        </div>
    </div>
</nav>
<section id="about">
    <div class="container">
        <?= $mainContent ?>
    </div>
</section>
<!-- Footer -->
<footer class="py-5 bg-dark">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Sokol Sergey 2018</p>
    </div>
    <!-- /.container -->
</footer>

<script src="<?= asset('js/jquery-3.3.1.min.js')?>" crossorigin="anonymous"></script>
<script src="<?= asset('js/popper.min.js')?>" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ"
        crossorigin="anonymous"></script>
<script src="<?= asset('js/bootstrap.min.js')?>" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm"
        crossorigin="anonymous"></script>
<script src="<?= asset('js/fileUploader/jquery.FileUploader.v2.js')?>" crossorigin="anonymous"></script>
<script src="<?= asset('js/swal/sweetalert2.min.js')?>" crossorigin="anonymous"></script>
<script src="<?= asset('js/main.js')?>" crossorigin="anonymous"></script>
</body>
</html>