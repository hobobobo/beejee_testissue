<div class="row">
    <div class="col-lg-8 mx-auto">


        <h2>Login</h2>
        <?= errorAlertsBlock($errorMessages); ?>


        <form name="sentMessage" id="postForm" novalidate="novalidate" action="?" method="post">
            <div class="control-group">
                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                    <label>Login</label>
                    <input class="form-control"
                           name="login" id="login" type="text"
                           placeholder="Login" required="required"
                           data-validation-required-message="Please enter your login."
                           value="<?= htmlspecialchars($login) ?>">
                    <p class="help-block text-danger"></p>
                </div>
            </div>
            <div class="control-group">
                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                    <label>password Address</label>
                    <input class="form-control"
                           name="password" id="password" type="password"
                           required="required"
                           data-validation-required-message="Please enter your password."
                           value="">
                    <p class="help-block text-danger"></p>
                </div>
            </div>
            <br>
            <div id="success"></div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-xl" id="sendMessageButton">Send</button>
            </div>
        </form>

    </div>
</div>