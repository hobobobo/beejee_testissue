<div class="row">
    <div class="col-lg-8 mx-auto">


        <h2>Edit Task #<?= $id ?></h2>
        <?= errorAlertsBlock($errorMessages); ?>


        <form name="sentMessage" id="taskForm" 1312novalidate="novalidate" action="?" method="post">
        <input type="hidden" name="id" value="<?= $id ?>">
            <div class="control-group">
                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                    <label>Name</label>
                    <input class="form-control"
                           name="name" id="name" type="text"
                           placeholder="Name" required="required"
                           data-validation-required-message="Please enter your name."
                           value="<?= htmlspecialchars($dataArray['name']) ?>">
                    <p class="help-block text-danger"></p>
                </div>
            </div>
            <div class="control-group">
                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                    <label>Email Address</label>
                    <input class="form-control"
                           name="email" id="email" type="email"
                           placeholder="Email Address" required="required"
                           data-validation-required-message="Please enter your email address."
                           value="<?= htmlspecialchars($dataArray['email']) ?>">
                    <p class="help-block text-danger"></p>
                </div>
            </div>
            <div class="control-group" id="upload_file" data-url2upload="<?= route('file.upload')?>" data-rec_id="<?= $id; ?>" data-model="task">
                <label>Image:</label>
                <ul class="existsfiles"><!-- for uploaded files -->
                    <?php
                    if ($dataArray['img'] ) {
                        echo '<li data-id="'.$dataArray['img'].'" data-uid="'.md5($dataArray['img'].'hash').'" data-delete="'.route('file.delete').'?id='.$dataArray['img'].'&model=task&rec_id='.$id.'"><a href="'.route('file.read').'?id='.$dataArray['img'].'"><img src="'.route('file.read').'?id='.$dataArray['img'].'"></a><button class="btn btn-danger btn-sm btn-delete-file" >X</button></li>';
                    }
                    ?>
                </ul>
                <div class="dropzone"> <!-- drop zone for uploading files -->
                    <input type="hidden" class="add_file_id" name="added_file" value="">
                    <ul class="addfileslist">
                    </ul>
                    <!--<button type="button" class="btn-upload-all btn btn-success btn-xs"><i class="glyphicon glyphicon-upload"></i>Завантажити</button>-->
                    <label for="upload_id" class="btn btn-outline-primary  fileinput-button">
                        <span>Choose Or Drop file</span>
                        <input id="upload_id" class="uploadfile" type="file" multiple name="added_file">
                    </label>
                </div>
            </div>
            <div class="control-group">
                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                    <label>Text</label>
                    <textarea class="form-control"
                              name="text" id="text" rows="5" placeholder="text" required="required"
                              data-validation-required-message="Please enter a text."><?= htmlspecialchars($dataArray['text']); ?></textarea>
                    <p class="help-block text-danger"></p>
                </div>
            </div>
            <br>
            <div id="success"></div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-xl" id="sendMessageButton">Send</button>
                <button type="button" class="btn btn-secondary btn-xl" id="btnPreview">Preview</button>
            </div>
        </form>
        <div class="preview hiddenblock">
            <div class="row">
                <div class="col-md-12" id="preview_block">
                    <span id="preview_img" class="float-right"></span>
                    Status: <span id="preview_status"></span><br>
                    Name: <span id="preview_name"></span><br>
                    Email: <span id="preview_email"></span><br>
                    <span id="preview_text"></span>

                </div>
                <button type="button" class="btn btn-secondary btn-xl" id="btnPreviewClose">Back</button>

            </div>
        </div>

    </div>
</div>