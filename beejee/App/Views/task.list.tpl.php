<div class="row">
    <div class="col-lg-8 mx-auto">

        <?php
            if(\Core\Request::get('success')) {
                echo '<div class="alert alert-success autohide" role="alert" >The operation was successful</div>';
            }
        ?>

        <h2>Task List</h2>



        <div class="btn-group" role="group" aria-label="Order by">
            <a class="btn btn-light <?= ($order=='name'?'active':'') ?>" href="<?= '?from=0&order=name&order_direction='.intval(!$order_direction) ?>" role="button">Name <span class="order__direction small"><?= $order_direction?'A-z':'Z-a' ?></span></a>
            <a class="btn btn-light <?= ($order=='email'?'active':'') ?>" href="<?= '?from=0&order=email&order_direction='.intval(!$order_direction) ?>" role="button">Email <span class="order__direction small"><?= $order_direction?'A-z':'Z-a' ?></span></a>
            <a class="btn btn-light <?= ($order=='is_done'?'active':'') ?>" href="<?= '?from=0&order=is_done&order_direction='.intval(!$order_direction) ?>" role="button">Status <span class="order__direction small"><?= $order_direction?'A-z':'Z-a' ?></span></a>
        </div>


        <div class="tasklist">
            <?php
                foreach ($tasks as $task) {

                    $status = $task['is_done']?'done':'-';
                    $adminControls = '';
                    if (\Core\Auth::getInstance()->isLogined()) {
                        $status  = '<a class="btn btn-outline-primary btn-approve" data-is_done="'.$task['is_done'].'" href="'.route('task.approve').'?id='.$task['id'].'">'.($task['is_done']?'done':'-').'</a><br>';
                        $adminControls = '<div class="text-center"><a class="btn btn-outline-primary " href="'.route('task.edit').'?id='.$task['id'].'">Edit</a></div>';
                    }
                    $img = '';
                    if ($task['img']) {
                        $img = '<img src="'.route('file.read').'?id='.$task['img'].'" alt=""  >';
                    }
                    echo '<div class="row">
                            <div class="col-md-12">
                            '.$adminControls.'
                                <span class="float-right">'.$img.'</span>
                                
                                Status: '.$status  .'<br> 
                                Name: '.$task['name'].'<br>
                                Email: '.$task['email'].'<br>
                            '.nl2br($task['text']).'    
                            </div>
                        </div>';
                }
            ?>
        </div>

        <nav aria-label="Page navigation" <?= ($totalPages-1<=0)?'style="display: none;"':''; ?>>
            <ul class="pagination justify-content-center">
                <li class="page-item <?= $from==0?'disabled':''; ?>"><a class="page-link" href="<?=  '?from='.($from-1).'&order='.$order.'&order_direction='.$order_direction  ?>" <?=$from==0?'tabindex="-1"':''; ?>>Previous</a></li>
                <?php

                for($i=max(0, $from-3); $i<min($totalPages, $from+3); $i++) {
                    echo '<li class="page-item '.($i==$from?'active':'').'"><a class="page-link" href="?from='.($i).'&order='.$order.'&order_direction='.$order_direction.'">'.($i+1).'</a></li>';
                }
                ?>
                <li class="page-item <?= $from==$totalPages-1?'disabled':''; ?>"><a class="page-link" href="<?=  '?from='.($from+1).'&order='.$order.'&order_direction='.$order_direction  ?>" <?= $from==$totalPages-1?'tabindex="-1"':''; ?>>Next</a></li>
            </ul>
        </nav>

    </div>
</div>